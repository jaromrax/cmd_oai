#!/usr/bin/env python3

from fire import Fire
import  os
import requests

deepai_api_key = os.getenv("DEEPAI_API_KEY")

# print("KEY ===  ", deepai_api_key )
if (deepai_api_key == "" ) or (deepai_api_key == None ):
    with open( os.path.expanduser("~/.deepai.token") ) as f:
        res = f.readlines()[0].strip()
    if not res is None and res!="":
        deepai_api_key = res
    else:
        print("X... I need DEEPAI_API_KEY  set !!!!!")
        print("X... export DEEPAI_API_KEY=")
        sys.exit(1)
#deepai.api_key = deepai_api_key





def main( text ):
    API="text2img"
    print("i... using API=",API)
    r = requests.post(
        f"https://api.deepai.org/api/{API}",
        data={
            'text': text,
        },
        headers={'api-key': deepai_api_key}
    )
    print(r.json())



if __name__=="__main__":
    Fire(main)
