#!/usr/bin/env python3
"""

 I wanted to see openai_pipe working....
... this is a modification of the underlying flask code

gem install openai_pipe
"""

PYSCRIPT="/tmp/gpt_code.py"
PYSCRIPT_EXISTS = False

HELP = f"""
reset  ...
q quit   ...
ROLES:______________________________________
a assis  ... assistant mode
t trans  ... change the instructions to - become translator
d dalle  ... change the instructions to - become Image generator
c coder  ... change the instructions to - become python coder
COMMANDS:_________________________________________
r run    ... run the code in {PYSCRIPT}, extracted from the previous response
i image  ... generate image by dall-e. if no text, use the last prompt response.
e extend ... try to extend the last created image from 512 to 1024
s speak  ... speak the answers
"""


import  base64

import sys

from prompt_toolkit import prompt
from prompt_toolkit import PromptSession
from prompt_toolkit.history import FileHistory
from os.path import expanduser

import datetime as dt

import subprocess as sp

import time


import sys
from fire import Fire
import os

from openai import OpenAI
from flask import Flask, redirect, render_template, request, url_for
import json
# ---- download image
import requests
import shutil
import datetime as dt

import random
from console import fg,bg,fx

# !!!!!!!!!!!!!!!
#
# pip install --upgrade openai
#
#
#
from matplotlib import pyplot as plt
from matplotlib import image as mpimg

import tiktoken
import re

import io
from PIL import Image
import bs4, requests
#
# --------------------- from Nv7-Github googlesearch
#
from time import sleep
from bs4 import BeautifulSoup
from requests import get
# from .user_agents import get_useragent
import urllib

from bs4.element import Comment
import urllib.request

#-------------------------------------------- HERE VOICE


from fire import Fire
from piper import PiperVoice
import wave
import sys
import pyaudio

from pydub.effects import speedup
import numpy as np
import struct
from scipy.ndimage import shift
import matplotlib.pyplot as plt



myPromptSession = PromptSession(history = FileHistory(expanduser('~/.myhistory')))

task_assis = """You are the assistant that responds questions correctly and fulfills given tasks.
You use a short and brief language, you do not repeat yourself nor the users' questions.
You make the answers short but precise. """


task_coder = """You are python expert. The prompts instruct you to create a python code.
The code must be able to run from the commandline, for this use Fire and default values.
If the graph output is necessary, use matplotlib.
The code must be always safe and secure, no harm for the filesystem nor for the network.
You use a short and brief language, you do not repeat yourself nor the users' questions.
You make the answers short but precise. Only python code, no examples how to run the code from shell.
"""

task_dalle = """ You are an expert for construction of creative text-prompts for DALL-E
2 image generator. You provide a desciption of the image based on the user input. Always use
high quality photograph design.
 Here are the general instructions:

 Your prompt can be up to 400 characters long.
Make the most of this character limit and add in as many details as
you can in order to get the best results. ou need to tell AI what kind
of visual style you have in mind. Otherwise, you get
a generic rendition of the keywords. Clearly specify your preferred style in the prompt.
Get more specific about the style and the design is better.

There are possibilities like wide angle photograph for landscape or towns,
 studio lighting, sharp focus for portrait.

  Adding the location
 detail adds to the accuracy of the image. So, when you create your prompts ensure that
there are enough details about the main subject of the design as well as the background
 or the scene in which you would like your subject to be present. Color of the subject’s
 costume, background colors, or an overview of the color moods you would like can add
 value to your prompt. you can add the right descriptors to your color names like
 “bright pink” or “sky blue” or even use specific color names like “crimson red”
 and “magenta”. Better yet, if you have the HEX code of these colors, you can add
 them to your prompt to get the exact colors you have in mind.  Describe the emotions
 you wish to incorporate. After all, without a strong emotional quotient, your image
 lacks depth. Adding emojis can also help capture the expression more accurately when
 you have animals or human beings in the picture or even anthropomorphic subjects.

You can even create prompts with just emojis. But yes, the interpretation varies
 drastically and you might not always get the results you imagine. For this, you
 can add details about the composition of the image, parameters like:

Photo angles – a macro shot of the subject, or an overhead shot or a drone shot.
Lighting – studio lighting, indoor lighting, outdoor lighting, backlit shots.
Photo lens effects – fisheye lens, double exposure. Avoid complicated prompts.

These were the general instructions. Now you compose a text-prompt for DALL-E 2
 based on the user input.

You must not do these things:
 - give no information back to the user, else it damages the prompt
 - never apologize, else it  damages the prompt
 - never say please, else it damages the prompt
 - never use words like  'generate' or 'create', else it damages the prompt
 - never thank the user
"""


task_trans = """ You translate scientific text prompts from English to Czech.
Use clean and concise language.
Reformulate the translated text to be clean and fluent.
It contains specific nuclear physics terminology so
 - "beam" means "svazek" in Czech
 - "target" means "terč" in Czech
 - "beam time" means "urychlovačový čas" in Czech
 - "beam dump" means "zařízení pro zastavení svazku" in Czech
 - "is promissing" mean "je slibná" in Czech
"""


#================================================================= global variables

file_name = "" # global last image name
count = 0   # for code marking
code_buffer = "" # save code to /tmp
PIPER = ""

started_total = dt.datetime.now().replace(microsecond=0)
started_task = dt.datetime.now().replace(microsecond=0)

messages = [] # GLOBAL LIST FOR CHAT


limit_tokens =300 # restrict tokens to return... change in the conversation
client = None
#app = Flask(__name__)



# ===============================================================================================
# ===============================================================================================
# --------------------- from Nv7-Github googlesearch


def tag_visible(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
        return False
    if isinstance(element, Comment):
        return False
    return True


def text_from_html(body):
    soup = BeautifulSoup(body, 'html.parser')
    texts = soup.findAll(text=True)
    visible_texts = filter(tag_visible, texts)
    return u" ".join(t.strip() for t in visible_texts)







# ===============================================================================================
# ===============================================================================================
# --------------------- from Nv7-Github googlesearch


def gooreq(term, results, lang, start, proxies, timeout, image_isch):
    resp = get(
        url="https://www.google.com/search",
        headers={
            "User-Agent": get_useragent()
        },
        params={
            "q": term,
            "tbm": image_isch,
            "num": results + 2,  # Prevents multiple requests
            "hl": lang,
            "start": start,
        },
        proxies=proxies,
        timeout=timeout,
    )

    resp.raise_for_status()
    return resp


class SearchResult:
    def __init__(self, url, title, description):
        self.url = url
        self.title = title
        self.description = description

    def __repr__(self):
        return f"SearchResult(url={self.url}, title={self.title}, description={self.description})"


#def gsearch(term):
def gsearch(term, num_results=3, lang="en", proxy=None, advanced=False, sleep_interval=0, timeout=5, image_isch=None):
    escaped_term = urllib.parse.quote_plus(term) # make 'site:xxx.xxx.xxx ' works.
    print("i... escaped==", escaped_term)

    # Proxy
    proxies = None
    if proxy:
        if proxy.startswith("https"):
            proxies = {"https": proxy}
        else:
            proxies = {"http": proxy}

    # Fetch
    start = 0
    tries = 4
    while start < num_results:
        print("i... sending request ...",escaped_term)
        # Send request
        try:
            tries-=1
            resp = gooreq(escaped_term, num_results - start,
                          lang, start, proxies, timeout, image_isch)
            print("ok")
        except:
            print("X... problem in gooreq")
        soup = BeautifulSoup(resp.text, "html.parser")

        # Parse
        result_block = soup.find_all("div", attrs={"class": "g"})

        print(  "results: ",len(result_block)  )

        if tries<0: return ""
        if len(result_block) ==0:
            start += 1

        for result in result_block:
            # Find link, title, description
            link = result.find("a", href=True)
            title = result.find("h3")
            description_box = result.find(
                "div", {"style": "-webkit-line-clamp:2"})
            if description_box:
                description = description_box.text
                if link and title and description:
                    start += 1
                    if advanced:
                        yield SearchResult(link["href"], title.text, description)
                    else:
                        yield link["href"]


        sleep(sleep_interval)
        if start == 0:
            return []

    return term





# ===============================================================================================
# ===============================================================================================


def get_useragent():
    """

    taken from https://github.com/Nv7-GitHub/googlesearch

    'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',

    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.62',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/111.0'

    """
    _useragent_list = [
        'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
]

    return random.choice(_useragent_list)



# ===============================================================================================
# ===============================================================================================
# ===============================================================================================

def get_web_text( htmladdr ):
    """
    find userfult text uning ....
    https://stackoverflow.com/questions/1936466/how-to-scrape-only-visible-webpage-text-with-beautifulsoup?answertab=modifieddesc#tab-top
    """

    #htmladdr = "https://www.nytimes.com/interactive/2022/09/13/us/politics/congress-stock-trading-investigation.html"

    response = requests.get( htmladdr,headers={'User-Agent': 'Mozilla/5.0','cache-control': 'max-age=0'}, cookies={'cookies':''})

    print(response.text)
    print("______")
    soup = bs4.BeautifulSoup( response.text  )
    res = soup
    #res = soup.article.get_text(' ', strip=True)
    return res




# ===============================================================================================


def mark_code(text, colors ):
    """
    1.st it set cyan and default marks.
    2.nd it saves the code to /tmp
    """
    global count
    global code_buffer
    global PIPER

    #print(text)
    #print(text)

    # Define the pattern to match the pair of symbols
    pattern = "\s*```"

    # Define a counter to keep track of occurrences
    counter = 0

    # # Define a function to handle the replacement
    def replace(match ):
        nonlocal counter
        nonlocal colors  # from the mother function
        #print("...MATCH                   ",counter)
        counter+= 1
        if colors:
            bs=f"\n{fx.italic}{bg.default}{fg.lightcyan}```"    # code is in cyan
            es=f"\n```{fx.default}{bg.default}{fg.lightyellow}" # response is in yellow
        else:
            bs="\n#+begin_src "
            es="\n#+end_src"
        if counter % 2 == 1:
            return f"{bs}"   # return beginsource

            #return f"\n{fx.italic}{bg.default}{fg.lightcyan}{bs} "
            #return f"{fx.italic}{bg.default}{fg.lightcyan}#+begin_src python :results replace output :session test :exports results "
        else:
            return f"{es}"  # return endsource
            #return f"\n{es}{fx.default}{bg.default}{fg.lightyellow}"
            #return "{bg.default}{fg.default}"

    # Use re.sub() with the defined function to replace the occurrences
    new_text = re.sub( pattern ,replace, text)

    if colors:
        new_text = f"{fg.lightyellow}{new_text}{fg.default}"
    else:
        new_text = f"{new_text}"
    return new_text


# ===============================================================================================


# def count_tokens( text, model = "gpt-3.5-turbo" ):
def count_tokens( text, model = "gpt-4" ):
    """ My try to count tokens, theirs is better"""
    # for the worst model:
    tokens_per_message = 4  # every message follows <|start|>{role/name}\n{content}<|end|>\n
    tokens_per_name = 1  # -1
    add3 = 3
    if (model ==  "gpt-3.5-turbo") or (model == "gpt-4"):
        enc = tiktoken.get_encoding("cl100k_base")
        assert enc.decode(enc.encode(text)) == text
        # To get the tokeniser corresponding to a specific model in the OpenAI API:
        enc = tiktoken.encoding_for_model("gpt-4")
        aaa = enc.encode(text)
        print("i... tokens content = ", aaa)
        toks = len(aaa)
    else:
        toks = len(text)
    #print("i... a token # = ",toks)
    # adapt on the worst model
    toks+=tokens_per_message
    toks+=tokens_per_name
    toks+=add3
    print("i... b token # = ",toks)
    return toks


def num_tokens_from_messages(model="gpt-3.5-turbo-0613"):
#def num_tokens_from_messages(model="gpt-4"):
    """Return the number of tokens used by a list of messages."""
    global messages
    try:
        encoding = tiktoken.encoding_for_model(model)
    except KeyError:
        print("Warning: model not found. Using cl100k_base encoding.")
        encoding = tiktoken.get_encoding("cl100k_base")
    if model in {
        "gpt-3.5-turbo-0613",
        "gpt-3.5-turbo-16k-0613",
        "gpt-4-0314",
        "gpt-4-32k-0314",
        "gpt-4-0613",
        "gpt-4-32k-0613",
        }:
        tokens_per_message = 3
        tokens_per_name = 1
    elif model == "gpt-3.5-turbo-0301":
        tokens_per_message = 4  # every message follows <|start|>{role/name}\n{content}<|end|>\n
        tokens_per_name = -1  # if there's a name, the role is omitted
    elif "gpt-3.5-turbo" in model:
        print("Warning: gpt-3.5-turbo may update over time. Returning num tokens assuming gpt-3.5-turbo-0613.")
        return num_tokens_from_messages(messages, model="gpt-3.5-turbo-0613")

    elif "gpt-4" in model:
        print("Warning: gpt-4 may update over time. Returning num tokens assuming gpt-4-0613.")
        return num_tokens_from_messages(messages, model="gpt-4")
    else:
        raise NotImplementedError(
            f"""num_tokens_from_messages() is not implemented for model {model}. See https://github.com/openai/openai-python/blob/main/chatml.md for information on how messages are converted to tokens."""
        )
    num_tokens = 0
    for message in messages:
        num_tokens += tokens_per_message
        for key, value in message.items():
            num_tokens += len(encoding.encode(value))
            if key == "name":
                num_tokens += tokens_per_name
    num_tokens += 3  # every reply is primed with <|start|>assistant<|message|>
    return num_tokens





# ===============================================================================================



def showim( fname ):
    plt.title(fname)
    plt.xlabel("X pixel scaling")
    plt.ylabel("Y pixels scaling")

    image = mpimg.imread(fname)
    plt.imshow(image)
    plt.show()
    return



# ===============================================================================================


def get_api_key():
    openai_api_key = os.getenv("OPENAI_API_KEY")

    # print("KEY from ENV  ===  ", openai_api_key )
    if (openai_api_key == "" ) or (openai_api_key == None ):
        #print("X... {fg.red} NO KEY in ENV.... {fg.default}")
        with open( os.path.expanduser("~/.openai.token") ) as f:
            res = f.readlines()[0].strip()
        if not res is None and res!="":
            openai_api_key = res
        else:
            print("X... I need OPENAI_API_KEY  set !!!!!")
            print("X... {fg.red} export OPENAI_API_KEY= {fg.default}")
            sys.exit(1)

    # print("KEY ... final  ===  ", openai_api_key )
    #openai.api_key = openai_api_key
    return openai_api_key




# ===============================================================================================




#def g_askme( prompt ,  temp = 0.0 , model='gpt-4-0613', total_model_tokens = 4096*2-50 ):
def g_askme( prompt ,  temp = 0.0 , model='gpt-4-0613', total_model_tokens = 4096*2-50 ):

    global task_trans, task_dalle, task_coder, task_assis
    global messages
    global file_name # last imagename
    global PYSCRIPT, PYSCRIPT_EXISTS
    global started_task
    global PIPER
    global limit_tokens

    if prompt.strip() == "":
        now = dt.datetime.now().replace(microsecond=0)
        print(f" ... No text posted...just return...  task: {now-started_task}; total:{now-started_total}")
        return

    if prompt == "reset":
        print(f"{fg.red} Reseting memory... {fg.default}")
        messages = []
        return

    if prompt == "help" or prompt == "h":
        print(f"{fg.red} HELP... {fg.default}")
        print(HELP)
        return

    if prompt == "quit" or prompt == 'q':
        print(f"{fg.red} Quitting... {fg.default}")
        messages = []
        sys.exit(0)
        return


    if prompt.find("google")==0 or prompt.find("g ") == 0 or prompt=="g":
        print(f"{fg.red} Google search, NO AI ... {fg.default}", f"{prompt.find('g ')}")
        #messages = []
        gmgq = ""
        if len(prompt)>6 and prompt.find("google ")>=0:
            gmgq = prompt.split("google")[1].strip()
        elif len(prompt)>2 and prompt.find("g ")>=0:
            gmgq = prompt[2:].strip()
        else:
            if len(messages)>1:
                gmgq = messages[-1]["content"]
        print("i... asking google for:", gmgq)
        if len(gmgq)>1:
            #messages = []
            #messages.append( {"role":"system", "content": "You will receive the content of a webpage or more webpages. Remember the information, respond only with OK."}  )

            print("i... g+.:",gmgq)
            res = list( gsearch(gmgq) )
            print("i... search results:")
            #for i in res:
            #    print("    ",i)
            itot=len(res)
            inow = 0
            for i in res:
                inow+=1
                #if inow>1: break
                print(f"{fg.cyan} {inow}/{itot} {i} {fg.default}")
                #html = urllib.request.urlopen(i).read()
                #txt = text_from_html(html)
                #print(f"{fg.green}", txt, f"{fg.default}")
                #print( "OUTPUT LENGTH=",len(txt) )
                #messages.append( {"role":"user", "content": txt}  )
        else:
            print(" ... NO search string given")
        #sys.exit(0)
        return


    #-------------------------------------------------- SYSTEM PROMPT START

    if prompt == "assis" or prompt == 'a':
        print(f"{fg.red} Reseting instructions to be an assistant: {fg.default}")
        messages = []
        messages.append( {"role":"system", "content":task_assis }  )
        started_task = dt.datetime.now().replace(microsecond=0)
        return



    if prompt == "trans" or prompt == 't':
        print(f"{fg.red} Reseting instructions to be a translation expert: {fg.default}")
        messages = []
        messages.append( {"role":"system", "content":task_trans }  )
        started_task = dt.datetime.now().replace(microsecond=0)
        return



    if prompt == "coder" or prompt == 'c':
        print(f"{fg.red} Reseting instructions to be a python expert: {fg.default}")
        messages = []
        messages.append( {"role":"system", "content":task_coder }  )
        started_task = dt.datetime.now().replace(microsecond=0)
        return



    if prompt == "dalle" or prompt == 'd':
        print(f"{fg.red} Reseting instructions to be a DALL-E expert: {fg.default}")
        messages = []
        messages.append( {"role":"system", "content":task_dalle }  )
        started_task = dt.datetime.now().replace(microsecond=0)
        return

    if prompt.find("limit ")==0 or prompt.find('l ')==0 or prompt.find('l')==0:
        print(f"{fg.red} Limit tokens. Currently {limit_tokens} ... {fg.default}")
        lll = prompt.split(" ")
        if len(lll)==1:
            return
        limit_tokens = int(prompt.split(" ")[-1])
        print(f"{fg.red} Limit tokens. Now       {limit_tokens} ... {fg.default}")
        return


    # -------------------------------------------------- SYSTEM PRONPT END

    if prompt == "speak" or prompt == 's':
        if PIPER == "":
            print(f"{bg.red}{fg.white}  Speak all answers (English) !!!! {bg.default}{fg.default}")
            PIPER = "en"
        elif PIPER == "en":
            print(f"{bg.red}{fg.white}  Speak all answers (CZECH) !!!! {bg.default}{fg.default}")
            PIPER = "cs"
        else:
            print(f"{bg.red}{fg.white}  DONT Speak all answers !!!! {bg.default}{fg.default}")
            PIPER = ""
        return

    if prompt == "run" or prompt == 'r':
        print(f"{bg.red}{fg.white}  Run the previous script !!!! {bg.default}{fg.default}")
        src = ""
        if PYSCRIPT_EXISTS:
            with open(PYSCRIPT) as f:
                src=f.readlines()
        else:
            print(f"{bg.red}{fg.white}  NO  previous script !!!! {bg.default}{fg.default}")
            return

        print(f"{fg.yellowgreen}")
        for i in src:
            print(i.replace(r"\t","    ").rstrip())
        print(f"{fg.default}")
        if input("RUN THIS?  y/n  ")=="y":
            sp.run(['python3', PYSCRIPT])
        else:
            print("Answer was no.")
        return

    if prompt.find("image")==0 or prompt == 'i' or prompt.find("i ")==0:
        print(f"{fg.red}  DALL-E image (direct or previous prompt),  prompt starts with 'image':  {fg.default}")
        imgq = ""
        if len(prompt)>7 and (prompt.find("image")==0 or prompt.find("i ")==0 ):
            if prompt.find("image")==0:
                imgq = prompt.split("image")[1].strip()
            elif prompt.find("i ")==0:
                imgq = prompt.split("i ")[1].strip()
            else: imgq = ""
        if len(imgq)<5 and len(messages)>0:
            imgq = messages[-1]["content"]
        print(f"I am sending to DALL-E: {fg.green} {imgq} {fg.default}")
        if len(imgq)>3:
            g_askimage(  imgq )
        else:
            print("... no prompt")
        return

    if prompt.find("extend")==0 or prompt == 'e':
        print(f"{fg.red}  EXTEND  DALL-E, prompt is/starts with 'extend'  {fg.default}")
        fname2 = ""
        if len(fname2)>5: # NONSENESE!!!!!!!!!!!!!
            fname2 = prompt.split("extend")[1].strip()
        if len(fname2)<2 and len(messages)>0:
            fname2 = file_name

        print(f"I am extending image: {fg.green} {fname2} {fg.default}")

        g_ask_extendimage(  fname2 )
        return



    resdi = None #===========================

    if prompt.find('vision')==0 or prompt.find('v ') == 0 or prompt == 'v':
        print(f"{fg.red}  VISION TEST   {fg.default}")
        fname2 = ""
        if prompt.find("vision")==0  and len(prompt)>len("vision")+2:
            fname2 = prompt.split("vision")[1].strip()
        if prompt.find("v ")==0 and len(prompt)>4:
            fname2 = prompt.split("v ")[1].strip()
        if len(prompt)<2 and len(messages)>0:
            fname2 = file_name # last image


        if os.path.exists( fname2) :
            print(f"I am sending an image: {fg.green} {fname2} {fg.default}")
        else:
            print(f"...NO IMAGE .........: {fg.red} {fname2} {fg.default}")

        resdi = g_ask_vision( image_path = fname2  )
        #return



    if resdi is None:
        resdi = g_ask_chat( prompt, temp , model, num_tokens_from_messages()+len(prompt)+600 )
        #resdi = g_ask_chat( prompt, temp , model, num_tokens_from_messages()+len(prompt)+300 )
        #resdi = g_ask_chat( prompt, temp , model, total_model_tokens  )


    if resdi is None:
        return

    with open("conversations.org","a") as f:
        tnow = dt.datetime.strftime(dt.datetime.now(),'%Y/%m/%d %H:%M:%S')
        f.write(f"*** {prompt}\n")
        f.write(f" /{tnow} temperature={temp}/\n\n")
        print()
        nc = 0

        #for i in resdi["choices"]:
        if True:
            nc+=1
            resi = resdi #i["message"]["content"].lstrip()
            messages.append( {"role":"assistant", "content":resi} )

            resip = mark_code(resi, colors = True)
            resiw = mark_code(resi, colors = False) # for emacs


            print( resip,  end="\n\n")  ## P print
            f.write(f" {nc}) {resiw}\n\n") ## w write

            resco = resiw

            if resco.find("#+begin_src")>=0 and  resco.find("#+end_src")>10:
                #resco = re.sub("",r"^.+#+begin_src",  resco , flags=re.DOTALL)

                #print(f"{fg.yellowgreen}",resco,f"{fg.default}")

                resco = re.sub(  r'^.*#\+begin_src(.*?)\n', "",  resco, flags=re.DOTALL ) # ignore \n
                #print(f"{fg.lightgreen}",resco,f"{fg.default}")

                resco = re.sub( r'#\+end_src.*$',  "", resco , flags=re.DOTALL) # ignore \n
                #print(f"{fg.green}",resco,f"{fg.default}")
                with open( PYSCRIPT , "w" ) as f:
                    f.write(resco)
                PYSCRIPT_EXISTS = True

            # if no python script....
            else:
                models = {}
                models['en'] = "en_US-lessac-medium.onnx"
                models['cs'] = "cs_CZ-jirka-medium.onnx"
                if PIPER in models.keys() :
                    lang=PIPER
                    voice = PiperVoice.load( models[lang], config_path=f"{models[lang]}.json")
                    audio_stream = voice.synthesize_stream_raw(resi, length_scale=0.8)
                    p = pyaudio.PyAudio()
                    stream = p.open(format=pyaudio.paInt16, channels=1,  rate=22050, output=True)
                    for audio_bytes in audio_stream:
                        stream.write( audio_bytes )
                    stream.stop_stream()
                    stream.close()
                    p.terminate()

            # break


    max_tokens = total_model_tokens - num_tokens_from_messages()

    now = dt.datetime.now().replace(microsecond=0)
    print(f"i...  remaining tokens= {max_tokens}; task: {now-started_task}; total:{now-started_total}" )

    return



# ===============================================================================================



def g_ask_extendimage(file_name):
    #openai.api_key = get_api_key()  # os.getenv("OPENAI_API_KEY")

    #ans = input("Extend y/n?")
    #ans = ans in ['y','Y']
    ans = True
    #print(ans, ans in ['y','Y'] )

    background = Image.open("mask.png")
    foreground = Image.open(file_name)

    foreground = foreground.resize((512, 512))
    # Calculate the position to place the foreground image
    x = (background.width - foreground.width) // 2
    y = (background.height - foreground.height) // 2
    # Paste the foreground image onto the background image
    background.paste(foreground, (x, y))
    # Save the resulting image
    background.save( "toextend.png" )

    prompt = file_name[:-3]
    while ans:
        response2 = client.images.generate(
            image=open( "toextend.png", "rb"),
            mask=open("mask.png", "rb"),
            prompt="Extend the image, continue seamlesly in the missing parts with continuous background, high detail, high resolution",
            n=1,
            size="1024x1024"
        )
        resdi2 = json.loads( str(response2)  )
        print("i...  response is arrived....")
        ni2 = 0
        for i2 in resdi2["data"]:
            ni2+=1
            url2 = i2["url"]
            if len(prompt)>100:
                fprompt2=prompt[:100]# +f"{int(random.random()*1000)}"
            else:
                fprompt2 = prompt

            fprompt2 = fprompt2.replace("IMAGES/","")
            #fprompt2 = fprompt2.replace("IMAGES/","")
            file_name2 = f"IMAGES/{dt.datetime.now().strftime('%Y%m%d_%H%M%S')}_X_{fprompt2}_{ni2}.png"
            print( "\nURL:\n",url2, "\n",file_name2, end="\n\n")

            ires2 = requests.get(url2, stream = True)
            if ires2.status_code == 200:
                print("i... saving:", file_name2)
                with open(file_name2,'wb') as f:
                    shutil.copyfileobj(ires2.raw, f)

                print('Image sucessfully Downloaded: ',file_name2)
                showim( file_name2 )
        ans = input("Another attempt to extend ?  y/n  >")
        ans = ans in ['y','Y']



# ===============================================================================================



# Function to encode the image
def encode_image(image_path):
    with open(image_path, "rb") as image_file:
        return base64.b64encode(image_file.read()).decode('utf-8')


def g_ask_vision(image_path, prompt="What’s in this image?"):
    """
    """
    api_key = get_api_key()  # os.getenv("OPENAI_API_KEY")

    # Getting the base64 string
    base64_image = encode_image(image_path)

    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {api_key}"
    }

    payload = {
        "model": "gpt-4-vision-preview",
        "messages": [
            {
                "role": "user",
                "content": [
                    {
                        "type": "text",
                        "text": prompt
                    },
                    {
                        "type": "image_url",
                        "image_url": {
                            "url": f"data:image/jpeg;base64,{base64_image}"
                        }
                    }
                ]
            }
        ],
        "max_tokens": 300
    }

    response = requests.post("https://api.openai.com/v1/chat/completions", headers=headers, json=payload)

    res = response.json()
    # response = client.chat.completions.create(
    #     model="gpt-4-vision-preview",
    #     messages=[
    #         {
    #             "role": "user",
    #             "content": [
    #                 {"type": "text", "text": prompt},
    #                 {
    #                     "type": "image_url",
    #                     "image_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Gfp-wisconsin-madison-the-nature-boardwalk.jpg/2560px-Gfp-wisconsin-madison-the-nature-boardwalk.jpg",
    #                 },
    #             ],
    #         }
    #     ],
    #     max_tokens=300,
    # )
    # res = response.choices[0]

    #print(res)
    #res = res['choices'][0]
    #print(res)
    #res = res['message']
    #res = res['content']
    #print(res)

    return res

# ===============================================================================================

def g_ask_chat( prompt ,  temp = 0.0 , model='gpt-4-0613', total_model_tokens = 4096*2-50 ):
    """
    CORE ChatGPT function
    """
    global task_assis, limit_tokens
    global client

    #openai.api_key = get_api_key()# os.getenv("OPENAI_API_KEY")   #print( os.getenv("OPENAI_API_KEY") )


    # ---- if no system present, add it
    if len(messages)==0:
        messages.append( {"role":"system", "content":task_assis }  )
    # add the message~
    messages.append( {"role":"user","content":prompt} )

    #tokens_prompt = count_tokens(prompt)
    #tokens_task = count_tokens(task)
    #max_tokens = 4096 - len(prompt) - len(task)
    #max_tokens = 4096 - tokens_prompt - tokens_task

    #max_tokens = 4096 - num_tokens_from_messages()
    max_tokens = total_model_tokens - num_tokens_from_messages()
    if limit_tokens<max_tokens:
        max_tokens = limit_tokens
    now = dt.datetime.now().replace(microsecond=0)

    print(f"i...  max_tokens= {max_tokens}, model = {model};  task: {now-started_task}; total:{now-started_total}" )

    # THIS CAN OBTAIN ERROR: enai.error.RateLimitError: Rate limit reached for 10KTPM-200RPM in organization org-YaucYGaAecppFiTrhbnquVvB on tokens per min. Limit: 10000 / min. Please try again in 6ms.
    # token size block it

    waittime = [1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3 ]
    DONE = len(waittime)-1
    responded = False
    while DONE>0: # I forgot to decrease
        DONE-=1
        time.sleep( 1*waittime[0] )
        try:
            # print(messages)
            print(" --> ")
            response = client.chat.completions.create(
                #model="gpt-3.5-turbo",
                model=model,
                messages=messages ,
                temperature=temp,
                max_tokens=max_tokens
            )
            print(" >>OK")
            DONE = 0
            responded = True
            # print(response)
        except Exception as e:
            print("An error occurred:", str(e))
            print(f"i... re-trying {DONE+1}x more time ... after {waittime[0]} seconds ...")
            time.sleep( 1*waittime[0] )
            waittime.pop(0)
            DONE-=1

    #print("i... OK SENT")
    if not responded:
        print("i... NOT RESPONDED  ====================================")
        return None

    #print(type(response))
    #print(str(response))

    resdi = response.choices[0].message.content

    #resdi = json.loads( str(response)  )
    return resdi
    #print(resdi)


    # ========================================================================

def g_askimage(prompt):
    "DALLE-2  DALLE-3 now"
    global file_name
    global client
    #openai.api_key = get_api_key()  # os.getenv("OPENAI_API_KEY")

    xsize="1792"
    ysize="1024"

    #response = openai.Image.create(
    #    prompt=prompt,
    #    n=1,
    #    size=f"{size}x{size}"
    #    #size="1024x1024"
    #)
#    response = client.images.generate(
    response = client.images.generate(
        model="dall-e-3",
        prompt=prompt,
        size=f"{xsize}x{ysize}",
        # quality="standard",
        quality="hd",
        n=1,
    )
    # image_url = response.data[0].url #DALLE3

    #print(response)
    resdi = json.loads( str(response)  )
    ni = 0
    for i in resdi["data"]:
        ni+=1
        url = i["url"]
        if len(prompt)>100:
            fprompt=prompt[:100]# +f"{int(random.random()*1000)}"
        else:
            fprompt = prompt


        file_name = f"IMAGES/{dt.datetime.now().strftime('%Y%m%d_%H%M%S')}_{fprompt}_{ni}.png"
        print( url, "\n",file_name, end="\n\n")

        ires = requests.get(url, stream = True)
        if ires.status_code == 200:

            #png_image = Image.open(io.BytesIO(ires.raw))
            #jpg_image = png_image.convert('RGB')
            #jpg_image.save( f"file_name.{jpg}", format='JPEG')

            with open(file_name,'wb') as f:
                shutil.copyfileobj(ires.raw, f)
            print('Image sucessfully Downloaded: ',file_name)



            showim( file_name)

            return
            ans = input("Extend y/n?")
            ans = ans in ['y','Y']
            #print(ans, ans in ['y','Y'] )
            if ans:
                background = Image.open("mask.png")
                foreground = Image.open(file_name)
                foreground = foreground.resize((512, 512))
                # Calculate the position to place the foreground image
                x = (background.width - foreground.width) // 2
                y = (background.height - foreground.height) // 2
                # Paste the foreground image onto the background image
                background.paste(foreground, (x, y))
                # Save the resulting image
                background.save( "toextend.png" )

            while ans:
                response2 = client.images.generate(
                    image=open( "toextend.png", "rb"),
                    mask=open("mask.png", "rb"),
                    prompt="Extend the image, fill the missing parts, high detail, high resolution",
                    n=1,
                    size="1024x1024"
                )
                resdi2 = json.loads( str(response2)  )
                print("i...  response is here....")
                ni2 = 0
                for i2 in resdi2["data"]:
                    ni2+=1
                    url2 = i2["url"]
                    if len(prompt)>100:
                        fprompt2=prompt[:100]# +f"{int(random.random()*1000)}"
                    else:
                        fprompt2 = prompt


                    file_name2 = f"IMAGES/{dt.datetime.now().strftime('%Y%m%d_%H%M%S')}_X_{fprompt2}_{ni2}.png"
                    print( url, "\n",file_name2, end="\n\n")

                    ires2 = requests.get(url2, stream = True)
                    if ires2.status_code == 200:
                        with open(file_name2,'wb') as f:
                            shutil.copyfileobj(ires2.raw, f)
                        print('Image sucessfully Downloaded: ',file_name2)
                        showim( file_name2 )
                ans = input("Extend once more y/n?")
                ans = ans in ['y','Y']

        #-----------------------------------------------------------------
        else:
            print('Image Couldn\'t be retrieved')

    return





def main(  demand="" ):
    global started_task, started_total
    #print(f"=/{demand}/=")
    #return
    #print( f"demand==/{demand}/ ... IMG={image}")


    if demand == "":
        while True:
            #print("> ", end="")
            inp = myPromptSession.prompt('> ')
            print()
            print(fg.white)
            print(inp)
            print(fg.default)
            #print()
            #print("{}, ok...".format(inp))

            g_askme( inp , model = "gpt-4")
            #askme( inp , model = "gpt-3.5-turbo")


    else:
        g_askme( demand, model = 'gpt-4')

# =======================================================================
# =======================================================================
# =======================================================================
# =======================================================================
# =======================================================================
# =======================================================================

if __name__ == "__main__":
    print( HELP )
    # list models
    client = OpenAI(api_key=get_api_key() )
    # openai.api_key = get_api_key()
    models = client.models.list()
    for i in models.data:
        if i.id.find("gpt")>=0: print(i.id)
    # print the first model's id
    # print(models.data[0].id)


    Fire( main )


# Write the e-lisp interactive function for emacs, that will find a definition for the selected text and inserts it in the current buffer without deleting anything.
