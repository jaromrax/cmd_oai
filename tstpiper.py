#!/usr/bin/env python3
#
#
# apt install portaudio19-dev
# ??? pip3 install pyaudio --upgrade
# pip3 install pyaudio==0.2.12 --upgrade
#
#
# RUN
#
#echo 'Welcome to the world of speech synthesis!' | piper   --model en_US-lessac-medium   --output_file welcome.wav
# echo 'Vítej do světa strojové řeči.' | piper   --model cs_CZ-jirka-medium   --output_file welcomecs.wav

# https://stackoverflow.com/questions/53797199/perform-fft-for-every-second-on-wav-file-with-python




from fire import Fire
from piper import PiperVoice
import wave
import sys
import pyaudio

from pydub.effects import speedup
import numpy as np
import struct
from scipy.ndimage import shift
import matplotlib.pyplot as plt

models = {}
models['en'] = "en_US-lessac-medium.onnx"
models['cs'] = "cs_CZ-jirka-medium.onnx"


def main( lang='cs'):
    print()
    voice = PiperVoice.load( models[lang], config_path=f"{models[lang]}.json")

    text = 'Example text'
    text = """Pomalu, ale přece.
"""
    #Ve světle klesající inflace v Česku začínají některé potraviny v obchodech v porovnání se začátkem roku viditelně zlevňovat. U vajec či másla šly ceny výrazně dolů, a to přes 25 procent."""

    #with wave.open('test.wav', "wb") as wav_file:
    #    voice.synthesize(text, wav_file)
    audio_stream = voice.synthesize_stream_raw(text)
    print( 'i... generator:',type(audio_stream) )

    #for audio_bytes in audio_stream:
    #    sys.stdout.buffer.write(audio_bytes)
    #    sys.stdout.buffer.flush()

    p = pyaudio.PyAudio()    # create an audio object

    # open stream based on the wave object which has been input.
    fs_rate = 22050
    stream = p.open(format=pyaudio.paInt16, #pyaudio.paFloat32, pyaudio.paInt16
                    channels=1,
                    rate=int(fs_rate*1.),
                    output=True)

    # i = 0
    # chunk=1024

    # data = audio_stream[:chunk]
    # while data:
    #     stream.write(data)
    #     i += chunk
    #     data = pd[i:i + chunk]._data

    for audio_bytes in audio_stream:
        print('i... bytes:',type(audio_bytes), len(audio_bytes)  )
        #audio_bytes = audio_bytes[0:2048]
        #print('i...      :',type(audio_bytes), len(audio_bytes)  )

        audio_bytes = np.frombuffer(audio_bytes, dtype=np.int16)

        signal = audio_bytes
        # generate time in seconds
        t = np.arange(signal.shape[0]) / fs_rate

        # create some plots
        fig, ax = plt.subplots(
            3, sharex=False, sharey=False,
            figsize=(8, 6))

        # plot everything
        ax[0].plot(t, signal);

        #for ax, i in zip(axs, range(0, signal.shape[0], fs_rate)):
        #    # pull out sample for this second
        #    ss = signal[i:i + fs_rate]

        # generate FFT and frequencies
        sp = np.fft.fft(signal)
        freq = np.fft.fftfreq(len(signal), 1 / fs_rate)

        # plot the first few components
        ax[1].plot(freq, np.abs(sp.real) );
        #ax[1].plot(freq, sp.real );


        data = np.fft.rfft( audio_bytes )
        print( type(data) , data.shape , data[:3] )

        #freqs = np.fft.fftfreq(len(data))
        #data = shift( data.real, -100,  cval = 0.0 )


        mid = int(len(data)/2)
        print( "datalen", len(data) , mid)

        # cut
        cut = 0
        if cut>0:
            data = np.roll(data, -cut)
            data[-cut:] = 0.0
        # for i in range(len(data) ):
        #     if i+cut<len(data):
        #         data.real[i]=data.real[i+cut]
        #     else:
        #         data.real[i]=0
        # for i in range(mid, len(data) ):
        #     # jde to od vysokych dolu,
        #     if i+cut<len(data):
        #         data.real[i] = data.real[i + cut]
        #         data.real[len(data)-i] = data.real[len(data) - i - cut]
        #     else:
        #         data.real[i] = 0
        #         data.real[len(data)-i] = 0
        #     # if i>mid+7000:
        #     #      pass
        #     # else:
        #     #     data[i] = 0
        #     #     data[-i] = 0


        print( type(data) , data.shape , data[:3] )
        ndata = np.fft.irfft( data )


        sp = np.fft.fft(ndata)
        freq = np.fft.fftfreq(len(ndata), 1 / fs_rate)
        #ax[2].plot(freq, np.abs(sp.real) );
        ax[2].plot( data.real );
        #ax[2].plot(freq[mid:], sp.real[mid:] );

        #plt.show()

        dataout = np.array(ndata *1.0, dtype=np.int16) #undo the *2 that was done at reading
        chunkout = struct.pack("%dh"%(len(dataout)), *list(dataout)) #convert back to 16-bit data
        stream.write(chunkout)

        #stream.write( audio_bytes )

    stream.stop_stream()
    stream.close()
    p.terminate()


if __name__=="__main__":
    Fire(main)
